from django import forms
from django.utils.translation import gettext_lazy as _

class SignupForm(forms.Form):
    first_name = forms.CharField(label=_("Vorname"), required=True)
    last_name = forms.CharField(label=_("Nachname"), required=False)
    email = forms.EmailField(label=_("E-Mail"), required=True)
    mobile = forms.CharField(label=_("Handy"), required=False)
