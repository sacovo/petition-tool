from django.db import models
from django.core.validators import int_list_validator
from django.contrib.auth.models import Group, User
from autoslug import AutoSlugField
from martor.models import MartorField

from sorl.thumbnail import ImageField
from django.utils.translation import gettext_lazy as _

import urllib.parse



class Petition(models.Model):
    title = models.CharField(max_length=280, verbose_name=_("Titel"))
    slug = AutoSlugField(populate_from='title', editable=True)

    lead = models.TextField(max_length=280, verbose_name=_("Call to Action"))
    text = MartorField(_("Text"))

    thankyou_text = MartorField(_("Dankestext"))
    whatsapp_text = models.TextField(_("Whatsapp Text"))

    header_img = ImageField(upload_to='headers/', verbose_name=_("Header Bild"))
    facebook_img = ImageField(upload_to='fb/', verbose_name=_("Facebook Vorschaubild"))

    facebook_site = models.CharField(max_length=200, blank=True, verbose_name=_("Facebook-Seite"))

    mailchimp_list_id = models.CharField(blank=True, max_length=300)
    mailchimp_api_key = models.CharField(blank=True, max_length=300)

    facebook_pixel = models.CharField(max_length=80, blank=True)
    google_tracking = models.CharField(max_length=80, blank=True)

    created_by = models.ForeignKey(User, models.CASCADE)
    group = models.ForeignKey(Group, models.CASCADE)

    webhook = models.URLField(max_length=2000, blank=True)

    is_active = models.BooleanField(default=True, verbose_name=_("Ist aktiv"))

    created = models.DateTimeField(auto_now_add=True)

    privacy_notice = models.CharField(max_length=280)
    privacy_text = MartorField(blank=True, verbose_name=_("Datenschutztext"))

    goal = models.IntegerField(default=100)

    additional_signatures = models.IntegerField(default=39)

    donation_amounts = models.CharField(max_length=180, default="10,20,50,100", validators=[int_list_validator])

    sign_now_text = models.CharField(max_length=180, default="Jetzt unterzeichnen!")

    def donation_amount_list(self):
        return self.donation_amounts.split(',')

    def count(self):
        return self.signature_set.count() + self.additional_signatures

    def whatsapp_link(self):
        return 'https://wa.me/?text=' + urllib.parse.quote(self.whatsapp_text)

    def missing(self):
        return self.goal - (self.signature_set.count() + self.additional_signatures)

    def achieved_percent(self):
        return ((self.signature_set.count() + self.additional_signatures) / self.goal) * 100

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['-created']
        verbose_name = _("Petition")
        verbose_name_plural = _("Petitionen")


class Signature(models.Model):
    first_name = models.CharField(max_length=200, verbose_name=_("Vorname"))
    last_name = models.CharField(max_length=200, verbose_name=_("Nachname"))
    email = models.EmailField(_("E-Mail"))
    mobile = models.CharField(max_length=200, blank=True, verbose_name=_("Handy"))
    petition = models.ForeignKey(Petition, models.CASCADE, verbose_name=_("Petition"))

    created = models.DateTimeField(auto_now_add=True, verbose_name=_("Erstellt"))

    class Meta:
        ordering = ['-created']
        verbose_name = _("Unterschrift")
        verbose_name_plural = _("Unterschriften")
        unique_together = ("petition", "email")

    def __str__(self):
        return self.petition.title + ': ' + self.first_name + ' ' + self.last_name


class Donation(models.Model):
    petition = models.ForeignKey(Petition, models.CASCADE, verbose_name=_("Petition"))
    amount = models.IntegerField(_("Betrag"))
    email = models.EmailField(_("E-Mail"))

    created = models.DateTimeField(auto_now_add=True, verbose_name=_("Erstellt"))

    class Meta:
        ordering = ['-created']
        verbose_name = _("Spende")
        verbose_name_plural = _("Spenden")

    def __str__(self):
        return self.petition.title + ": " + self.email + ', ' + str(self.amount)


class Subscription(models.Model):
    group = models.OneToOneField(Group, models.CASCADE)
    description = models.TextField()

    stripe_public_key = models.CharField(max_length=100, blank=True)
    stripe_private_key = models.CharField(max_length=100, blank=True)

    stripe_icon_url = models.URLField(blank=True)

    def get_fee(self, amount):
        return min([self.application_fee_max, self.application_fee_base + amount * self.application_fee_rel])

