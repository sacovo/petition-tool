from django.contrib import admin
from petition.models import *
from sorl.thumbnail.admin import AdminImageMixin
from modeltranslation.admin import TranslationAdmin



# Register your models here.


class SignatureInline(admin.TabularInline):
    model = Signature
    extra = 0

    readonly_fields = ['first_name', 'last_name', 'email', 'mobile', 'created']


class DonationInline(admin.TabularInline):
    model = Donation
    extra = 0

    readonly_fields = ['amount', 'email', 'created']


@admin.register(Petition)
class PetitionAdmin(AdminImageMixin, TranslationAdmin):
    inlines = [SignatureInline, DonationInline]

    list_display = ['title', 'slug', 'group', 'is_active', 'created', 'created_by']
    list_filter = ['group', 'created_by']
    date_hierarchy = 'created'

    fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('title','slug', 'group'),
        }),
        ('Lead', {
            'classes': ('wide',),
            'fields': ('lead', 'header_img', 'goal', 'additional_signatures', 'sign_now_text'),
        }),
        ('Text', {
            'classes': ('wide',),
            'fields': ('text',),
        }),
        ('Thank You', {
            'classes': ('wide',),
            'fields': ('thankyou_text',),
        }),
        ('Social Media', {
            'classes': ('collapse', 'wide'),
            'fields': ('whatsapp_text', 'facebook_img', 'facebook_site'),
        }),
        ('Mailchimp', {
            'classes': ('collapse',),
            'fields': ('mailchimp_list_id', 'mailchimp_api_key'),
        }),
        ('Tracking', {
            'classes': ('collapse',),
            'fields': ('facebook_pixel', 'google_tracking'),
        }),
        ('Advanced', {
            'classes': ('collapse',),
            'fields': ('webhook', 'is_active', 'privacy_text'),
        }),
    )

    def get_form(self, request, obj=None, **kwargs):
        form =  super().get_form(request, obj, **kwargs)
        if request.user.is_superuser:
            return form
        form.base_fields['group'].queryset = request.user.groups
        return form

    def save_model(self, request, obj, form, change):
        if not change:
            obj.created_by = request.user
        super().save_model(request, obj, form, change)

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if request.user.is_superuser:
            return queryset
        return queryset.filter(group__in=request.user.groups.all())


@admin.register(Subscription)
class SubscriptionAdmin(admin.ModelAdmin):
    pass

@admin.register(Signature)
class SignatureAdmin(admin.ModelAdmin):
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if request.user.is_superuser:
            return queryset
        return queryset.filter(petition__group__in=request.user.groups.all())

@admin.register(Donation)
class DonationAdmin(admin.ModelAdmin):
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if request.user.is_superuser:
            return queryset
        return queryset.filter(petition__group__in=request.user.groups.all())
