from modeltranslation.translator import translator, TranslationOptions
from petition.models import Petition


class PetitionTranslationOptions(TranslationOptions):
    fields = ('title','slug', 'lead', 'text', 'thankyou_text', 'whatsapp_text',
              'header_img', 'facebook_img', 'facebook_site', 'mailchimp_list_id',
              'mailchimp_api_key', 'privacy_text', 'sign_now_text')

translator.register(Petition, PetitionTranslationOptions)
