from django.urls import path

from petition.views import PetitionView, ThankYouView, PetitionOverview, PrivacyView, charge

urlpatterns = [
    path('<slug:slug>/', PetitionView.as_view(), name='petition-detail'),
    path('<slug:slug>/thanks/', ThankYouView.as_view(), name='petition-thanks'),
    path('<slug:slug>/privacy/', PrivacyView.as_view(), name='petition-privacy'),
    path('<slug:slug>/donate/', charge, name='petition-charge'),
    path('', PetitionOverview.as_view(), name='petition-overview')
]

