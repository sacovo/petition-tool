import stripe

from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.conf import settings
from django.db import IntegrityError
from django.utils import translation

from mailchimp3 import MailChimp
from mailchimp3.mailchimpclient import MailChimpError

from django.http import Http404


from .forms import SignupForm

import hashlib

from .models import *

# Create your views here.

def get_hash(email):
    email = email.lower()
    return hashlib.md5(email.encode('utf-8')).hexdigest()

class PetitionView(DetailView):
    model = Petition
    http_method_names = ['get', 'post']

    def post(self, request, **kwargs):
        return self.get(request, **kwargs)

    def process_form(self, data, **kwargs):
        try:
            Signature.objects.create(
                    first_name = data['first_name'],
                    last_name = data['last_name'],
                    email = data['email'],
                    mobile = data['mobile'],
                    petition = self.object,
            )
        except IntegrityError:
            print("Integrity Error: mail already exists")

        try:
            if self.object.mailchimp_api_key:
                client = MailChimp(mc_api=self.object.mailchimp_api_key)
                client.lists.members.create_or_update(self.object.mailchimp_list_id, get_hash(data['email']), {
                    'email_address': data['email'],
                    'status_if_new': 'subscribed',
                    'tags': [{'name': self.object.slug, 'status': 'active'}],
                    'merge_fields': {
                        'FNAME': data['first_name'],
                        'LNAME': data['last_name'],
                        'PHONE': data['mobile'],
                        },
                    })
                client.lists.members.tags.update(self.object.mailchimp_list_id, get_hash(data['email']), {
                    'tags': [{'name': self.object.slug, 'status': 'active'}],
                })
        except MailChimpError:
            print("Mailchimp Error")

        self.request.session['has_signed_' + str(self.object.pk)] = True

    def dispatch(self, request, *args, **kwargs):
        for language in settings.LANGUAGES:
            lookup = {'slug_' + language[0]: kwargs['slug']}
            query = Petition.objects.filter(**lookup)
            if query.count() == 1:
                translation.activate(language[0])
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        form = SignupForm()
        if self.request.POST:
            form = SignupForm(self.request.POST)
            if form.is_valid():
                self.process_form(form.cleaned_data)
        context['form'] = form
        context['has_signed'] = self.request.session.get('has_signed_' + str(self.object.pk), False)
        return context


class PrivacyView(DetailView):
    model = Petition

    template_name = 'petition/privacy_view.html'

    def dispatch(self, request, *args, **kwargs):
        for language in settings.LANGUAGES:
            lookup = {'slug_' + language[0]: kwargs['slug']}
            query = Petition.objects.filter(**lookup)
            if query.count() == 1:
                translation.activate(language[0])
        return super().dispatch(request, *args, **kwargs)

class ThankYouView(DetailView):
    model = Petition
    template_name = 'petition/thankyou_view.html'


class PetitionOverview(ListView):
    model = Petition
    paginate_by = 10


def charge(request, slug):
    for language in settings.LANGUAGES:
        lookup = {'slug_' + language[0]: kwargs['slug']}
        query = Petition.objects.filter(**lookup)
        if query.count() == 1:
            translation.activate(language[0])
    petition = get_object_or_404(Petition, slug=slug)
    stripe.api_key = petition.group.subscription.stripe_private_key
    amount = int(request.POST['amount'])
    if request.method == 'POST':
        charge = stripe.Charge.create(
                amount=amount,
                currency='chf',
                source=request.POST['stripeToken'],
                )

        Donation.objects.create(
                petition = petition,
                amount = amount,
                email = request.POST['stripeEmail'],
                )
        if petition.mailchimp_api_key:
            email = request.POST['stripeEmail']
            client = MailChimp(mc_api=petition.mailchimp_api_key)
            client.lists.members.create_or_update(petition.mailchimp_list_id, get_hash(email), {
                'email_address': email,
                'status_if_new': 'subscribed',
                'tags': [{'name': petition.slug + "-donated", 'status': 'active'}],
                'merge_fields': {},
                })
            client.lists.members.tags.update(petition.mailchimp_list_id, get_hash(email), {
                'tags': [{'name': petition.slug + "-donated", 'status': 'active'}],
            })
        return
